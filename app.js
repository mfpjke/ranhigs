const express = require('express');
const errorHandler = require('errorhandler');

const config = require('./config');
const security = require('./utils/security');
const settings = require('./utils/settings');
const api = require('./routes');

const port = process.env.PORT || config.port || 3000;

let instance = null;

class Application {
  constructor() {
    if (!instance) {
      this.app = express();

      this.setPort(port);

      this.use(security);
      this.use(settings);

      this.setApi(api);
      this.setErrorHandler(errorHandler);

      instance = this;
    }

    return instance;
  }

  use(subjects) {
    if (subjects && subjects instanceof Object) {
      Object.keys(subjects).forEach(sub => {
        this.app.use(subjects[sub]);
      })
    }
  }

  setApi(api) {
    if (api && api instanceof  Object) {
      Object.keys(api).forEach(method => {
        this.app.use(api[method].path, api[method].route);
      })
    }
  }

  setPort(port) {
    if (port) {
      this.app.set(port);
    }
  }

  setErrorHandler(errorHandler) {
    if (errorHandler) {
      this.app.use(errorHandler);
    }
  }
}

module.exports = new Application().app;
