const path = require('path');
const dotEnvPath = path.join(__dirname, '../.env');

require('dotenv').config({ silence: true, path: dotEnvPath });

const config = {
  databaseUri: process.env.DATABASE_URI,
  redisUri: process.env.REDIS_URI,
  port: process.env.PORT,
  protocol: process.env.PROTOCOL,
};

module.exports = config;
