const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/login', (req, res) => {
  const { email, password } = req.body;

  db.checkConnection()
    .then(() => {
      const userCollection = db.connection.collection('User');

      return userCollection
        .findOne({ email });
    })
    .then((user) => {
      let token;
      if (user && password === user.password) {
        token = jwt.sign(user, 'ranepa', { expiresIn: 60 * 60 });
        return Promise.resolve(token);
      } else {
        return Promise.reject({ message: errors.user_not_found });
      }
    })
    .then(token => res.json({ token }))
    .catch(err => res.json({ err }));
});

module.exports = router;
