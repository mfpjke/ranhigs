const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/questions', (req, res) => {
  const { token, type } = req.body;

  db.checkConnection()
    // .then(() => {
    //   return jwt.verify(token, 'ranepa');
    // })
    // .then(decoded => {
    //   return decoded
    //     ? Promise.resolve()
    //     : Promise.reject();
    // })
    .then(() => {
      const questionCollection = db.connection.collection('Question');

      return questionCollection
        .find({ type })
        .toArray();
    })
    .then(docs => res.json({
      token,
      questions: docs,
    }))
    .catch(err => {
      console.error(err);
      res.json({ message: errors.internal_error });
    });
});

module.exports = router;
