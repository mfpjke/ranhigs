const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/result', (req, res) => {
  const { token, questions, type } = req.body;

  db.checkConnection()
    // .then(() => {
    //   return jwt.verify(token, 'ranepa');
    // })
    // .then((decode) => {
    //   return decode
    //     ? Promise.resolve()
    //     : Promise.reject();
    // })
    .then(() => {
      const eduPathCollection = db.connection.collection('EduPath');

      const initialStage = {
        $project: {
          type: 1,
          faculty: 1,
          title: 1,
          programs: 1,
          baseEducation: 1,
          additionalTest: 1,
          _p_subjects: 1,
          _p_forms: 1,
        },
      };

      const unwindSubjectStage = [
        {
          $unwind: { path: '$_p_subjects' },
        },
        {
          $lookup: {
            from: 'SchoolSubject',
            localField: '_p_subjects',
            foreignField: '_id',
            as: 'eduPath',
          },
        },
        {
          $unwind: { path: '$eduPath', preserveNullAndEmptyArrays: true },
        },
      ];

      const unwindFormStage = [
        {
          $project: {
            faculty: 1,
            title: 1,
            programs: 1,
            baseEducation: 1,
            additionalTest: 1,
            _p_forms: 1,
            subject: '$eduPath.name',
          }
        },
        {
          $unwind: { path: '$_p_forms' }
        },
        {
          $lookup: {
            from: 'EduForm',
            localField: '_p_forms',
            foreignField: '_id',
            as: 'eduPath',
          }
        },
        {
          $unwind: { path: '$eduPath', preserveNullAndEmptyArrays: true },
        },
      ];

      const removeDuplicateStage = [
        {
          $project: {
            faculty: 1,
            title: 1,
            programs: 1,
            baseEducation: 1,
            additionalTest: 1,
            subject: 1,
            eduPath: 1,
          }
        },
        {
          $group: {
            _id: '$_id',
            faculty: { $first: '$faculty' },
            title: { $first: '$title' },
            additionalTest: { $first: '$additionalTest' },
            subject: { $addToSet: '$subject' },
            forms: { $first: '$eduPath' },
            programs: { $first: '$programs' },
            baseEducation: { $first: '$baseEducation' }
          }
        },
      ];

      const findDecision = [

      ];

      const aggregationQuery = [
        initialStage,
        ...unwindSubjectStage,
        ...unwindFormStage,
        ...removeDuplicateStage,
        ...findDecision,
      ];

      return eduPathCollection
        .aggregate(aggregationQuery)
        .toArray();
    })
    .then((docs) => res.json({
      token,
      educationPaths: docs,
    }))
    .catch(err => {
      console.error(err);
      res.json({ message: errors.internal_error });
    });
});

module.exports = router;
