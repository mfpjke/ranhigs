const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.get('/search', (req, res) => {
  const { token, type, eduType, price, query, schoolSubjects } = req.body;

  db.checkConnection()
    // .then(() => {
    //   return jwt.verify(token, 'ranepa');
    // })
    // .then(decoded => {
    //   return decoded
    //     ? Promise.resolve()
    //     : Promise.reject();
    // })
    .then(() => {
      const eduPathCollection = db.connection.collection('EduPath');

      const initialProject = {
        $project: {
          type: 1,
          faculty: 1,
          title: 1,
          programs: 1,
          baseEducation: 1,
          additionalTest: 1,
          _p_subjects: 1,
          _p_forms: 1,
        },
      };

      const unwindBySubjects = {
        $unwind: { path: '$_p_subjects' },
      };

      const lookupWithSubjects = [
        {
          $lookup: {
            from: 'SchoolSubject',
            localField: '_p_subjects',
            foreignField: '_id',
            as: 'eduPath',
          },
        },
        {
          $unwind: { path: '$eduPath', preserveNullAndEmptyArrays: true },
        },
      ];

      const groupById = {
        $group: {
          _id: '$_id',
          type: '$type',
          faculty: '$faculty',
          title: '$title',
          programs: '$programs',
          baseEducation: '$baseEducation',
          additionalTest: '$additionalTest',
          subjects: '$eduPath.name',
          sizeOfSubjects: { $size: '$_p_subjects' },
          countSizeOfSubjects: { $count: { $sum: 1 } },
          _p_forms: '$_p_forms',
        },
      };

      const filterBySubjects = [
        {
          $project: {
            type: 1,
            faculty: 1,
            title: 1,
            programs: 1,
            baseEducation: 1,
            additionalTest: 1,
            subjects: 1,
            _p_forms: 1,
            isSameSubjects: { $eq: { '$sizeOfSubjects': '$countSizeOfSubjects' } }
          },
        },
        {
          $match: {
            isSameSubjects: true,
          },
        }
      ];

      const lookupWithForms = [
        {
          $unwind: { path: '$_p_forms' }
        },
        {
          $lookup: {
            from: 'eduForm',
            localField: '_p_forms',
            foreignField: '_id',
            as: 'eduPath',
          }
        },
        {
          $unwind: { path: '$eduPath' },
        }
      ];

      // const matchResults = {
      //   $match: {
      //     type: type,
      //     subjects: schoolSubjects,
      //     title: eduType,
      //   }
      // };

      const aggregationPromises = [
        initialProject,
        unwindBySubjects,
        ...lookupWithSubjects,
        groupById,
        ...filterBySubjects,
        ...lookupWithForms,
        // matchResults,
      ];

      return eduPathCollection
        .aggregate(aggregationPromises)
        .toArray();
    })
    .then((docs) => res.json({
      token,
      educationPaths: docs,
    }))
    .catch(err => res.json({ message: err }));
});

module.exports = router;
