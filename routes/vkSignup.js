const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/vk_signup', (req, res) => {
  let userCollection;
  const { vkToken, vkId } = req.body;

  const newUser  = {
    email: null,
    password: null,
    vkToken,
    vkId,
    goToken: null,
  };

  db.checkConnection()
    .then(() => {
      userCollection = db.connection.collection('User');

      return userCollection
        .findOne({ vkId });
    })
    .then((user) => {
      if (!user) {
        userCollection.insertOne(newUser);
        return Promise.resolve();
      }
      return Promise.reject(errors.user_already_exists);
    })
    .then(() => {
      const token = jwt.sign(newUser, 'ranepa', { expiresIn: 60 * 60 });

      return Promise.resolve(token);
    })
    .then(token => res.json({ token }))
    .catch(err => res.json({ message: err }));
});

module.exports = router;
