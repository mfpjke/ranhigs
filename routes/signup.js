const express = require('express');
const router = express.Router();
const db = require('../database');
const bcrypt = require('bcrypt');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/signup', (req, res) => {
  let userCollection;
  const { email, password } = req.body;

  const newUser = {
    email,
    password: password,
    vkToken: null,
    goToken: null,
  };

  db.checkConnection()
    .then(() => {
      userCollection = db.connection.collection('User');

      return userCollection
        .findOne({ email });
    })
    .then((user) => {
      if (!user) {
        userCollection.insertOne(newUser);
        return Promise.resolve();
      }
      return Promise.reject(errors.user_already_exists);
    })
    .then(() => {
      const token = jwt.sign(newUser, 'ranepa', { expiresIn: 60 * 60 });

      return Promise.resolve(token);
    })
    .then(token => res.json({ token }))
    .catch(err => res.json({ message: err }));
});

module.exports = router;
