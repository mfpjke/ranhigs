const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');
const https = require('https');

router.post('/vk_login', (req, res) => {
  const { vkToken, vkId } = req.body;

  const url = `https://api.vk.com/method/users.get?access_token=${vkToken}&v=5.52`;

  // todo identify vkId by vkToken
  https.get(url, (response) => {
    response.on('data', (user) => {
      const json = JSON.parse(user.toString()).response[0];

      if (json.id.toString() === vkId) {
        db.checkConnection()
          .then(() => {
            const userCollection = db.connection.collection('User');

            return userCollection
              .updateOne(
                { vkId },
                { $set: { vkToken } }
              );
          })
          .then((user) => {
            let token;

            if (user) {
              token = jwt.sign(user, 'ranepa', { expiresIn: 60 * 60 });
              return Promise.resolve(token);
            } else {
              return Promise.reject({ message: errors.user_not_found });
            }
          })
          .then(token => res.json({ token }))
          .catch(err => res.json({ err }));
      }
    });
  })
  .on('error', (err) => {
    res.json({ err });
  })
});

module.exports = router;
