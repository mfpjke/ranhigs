const login = require('./login');
const logout = require('./logout');
const vk_login = require('./vkLogin');
const go_login = require('./goLogin');
const signup = require('./signup');
const vk_signup = require('./vkSignup');
const go_signup = require('./goSignup');
const questions = require('./questions');
const result = require('./result');
const search = require('./search');

const routes = [
  { path: '/', route: login },
  { path: '/', route: logout },
  { path: '/', route: vk_login },
  { path: '/', route: go_login },
  { path: '/', route: signup },
  { path: '/', route: vk_signup },
  { path: '/', route: go_signup },
  { path: '/', route: search },
  { path: '/', route: questions },
  { path: '/', route: result },
];

module.exports = routes;
