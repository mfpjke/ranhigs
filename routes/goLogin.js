const express = require('express');
const router = express.Router();
const db = require('../database');
const errors = require('../utils/errors');
const jwt = require('jsonwebtoken');

router.post('/go_login', (req, res) => {
  const { goToken } = req.body;

  db.checkConnection()
    .then(() => {
      const userCollection = db.connection.collection('User');

      return userCollection
        .findOne({ goToken });
    })
    .then((user) => {
      let token;

      if (user) {
        token = jwt.sign(user, 'ranepa', { expiresIn: 60 * 60 });
        return Promise.resolve(token);
      } else {
        return Promise.reject({ message: errors.user_not_found });
      }
    })
    .then(token => res.json({ token }))
    .catch(err => res.json({ err }));
});

module.exports = router;
