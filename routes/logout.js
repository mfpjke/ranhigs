const express = require('express');
const router = express.Router();

router.post('/logout', (req, res) => {
  req.logout()
    .then(() => req.logout())
    .then(() => req.redirect('/'))
    .then(() => res.sendStatus(200))
    .catch(err => {
      console.error(err);
      res.sendStatus(404);
    })
});

module.exports = router;
