const fs = require('fs');

function bootstrapHTTPS(app) {
  const https = require('https');
  const options = {
    key: fs.readFileSync('../certificates/key.pem', 'utf8'),
    cert: fs.readFileSync('../certificates/cert.pem', 'utf8'),
  };
  return https.createServer(options, app);
}

function bootstrapHTTP(app) {
  const http = require('http');
  return http.createServer(app);
}

module.exports = function configureServer(protocolName, application) {
  return protocolName === 'http'
    ? bootstrapHTTP(application)
    : bootstrapHTTPS(application);
};
