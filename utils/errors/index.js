const errors = {
  no_db_connection: { code: 1, text: 'No db connection' },
  no_access: { code: 209, text: 'No access' },
  internal_error: { code: 1, text: 'Internal server error' },
  no_session: { code: 102, text: 'No session' },
  invalid_id_type: { code: 102, text: 'Invalid identifier type' },
  invalid_id: { code: 1000, text: 'Invalid identifier' },
  invalid_params: { code: 102, text: 'Invalid params' },
  validation_error: { code: 102, text: 'Validation error' },
  user_not_found: { code: 404, text: 'User not found' },
  user_already_exists: { code: 400, text: 'User with this email already exist' }
};

module.exports = errors;
