const helmet = require('helmet');

const maxAgeHSTS = 60 * 60 * 24 * 7 * 18;

const security = {
  dnsPrefetchControl: helmet.dnsPrefetchControl(),
  frameguard: helmet.frameguard({ action: 'deny' }),
  hidePoweredBy: helmet.hidePoweredBy(),
  hsts: helmet.hsts({
    // google recommendation
    maxAge: maxAgeHSTS,
    includeSubDomains: true,
    preload: true,
  }),
  ieNoOpen: helmet.ieNoOpen(),
  noCache: helmet.noCache(),
  noSniff: helmet.noSniff(),
  xssFilter: helmet.xssFilter(),
};

module.exports = security;
