const EduForm = require('./EduForm');
const EduPath = require('./EduPath');
const Question = require('./Question');
const SchoolSubject = require('./SchoolSubject');
const User = require('./User');

const data = {
  EduForm,
  EduPath,
  Question,
  SchoolSubject,
  User,
};

module.exports = data;
