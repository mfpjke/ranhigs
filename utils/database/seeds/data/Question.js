const Questions = {
  rowList: [
    {
      multy: true,
      text: 'Какие предметы абитуриент сдает на ЕГЭ?',
      type: '11grade',
      answers: [
        { text: 'Математика' },
        { text: 'Обществознание' },
        { text: 'Русский язык' },
        { text: 'История' },
        { text: 'Литература' },
        { text: 'Биология' },
      ],
    },
    {
      multy: false,
      text: 'Согласен ли человек сдавать дополнительные экзамены?',
      type: '11grade',
      answers: [
        { text: 'Да' },
        { text: 'Нет' },
      ],
    },
    {
      multy: false,
      text: 'Какая форма обучения более предпочтительна для будущего студента?',
      type: '11grade',
      answers: [
        { text: 'Очная' },
        { text: 'Очно-заочная' },
        { text: 'Заочная' },
      ],
    },
    {
      multy: false,
      text: 'Рассматривает ли абитуриент поступление по контракту?',
      type: '11grade',
      answers: [
        { text: 'Да' },
        { text: 'Нет' },
      ],
    },
    {
      multy: true,
      text: 'К чему человек более склонен?',
      type: '11grade',
      answers: [
        { text: 'Финансы' },
        { text: 'Политика' },
        { text: 'Управление' },
        { text: 'Безопасность' },
        { text: 'Юриспруденция' },
        { text: 'Психология' },
        { text: 'Работа с социумом' },
      ],
    },
  ]
};

module.exports = Questions;
