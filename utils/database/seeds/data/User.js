const bcrypt = require('bcrypt');
const salt = bcrypt.genSaltSync(10);

console.log(`salt ${salt}`);

const Users = {
  rowList: [
    {
      email: 'harry@gmail.com',
      password: '123qwerty',
      vkToken: null,
      vkId: null,
      goToken: null,
      goId: null,
    },
    {
      email: 'root@outlook.com',
      password: 'root',
      vkToken: null,
      vkId: null,
      goToken: null,
      goId: null,
    },
    {
      email: 'hipster@icloud.com',
      password: 'hipstyhips',
      vkToken: null,
      vkId: null,
      goToken: null,
      goId: null,
    },
    {
      email: 'basic@beast.inside',
      password: 'wtfdata',
      vkToken: null,
      vkId: null,
      goToken: null,
      goId: null,
    },
    {
      email: 'typical@asshole.ass',
      password: '123123',
      vkToken: null,
      vkId: null,
      goToken: null,
      goId: null,
    },
  ]
};

module.exports = Users;
