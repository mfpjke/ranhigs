const SchoolSubjects = {
  rowList: [
    { name: 'Математика' },
    { name: 'Обществознание' },
    { name: 'Русский язык' },
    { name: 'История' },
    { name: 'Литература' },
    { name: 'Биология' },
  ]
};

module.exports = SchoolSubjects;
