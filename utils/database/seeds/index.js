module.exports = function seed(db, collections) {
  db.checkConnection()
    .then(() => {
      const promises = [];

      Object.keys(collections).forEach(collectionName => {
        const data = collections[collectionName];
        const currentCollection = db.connection.collection(collectionName);

        promises.push(
          currentCollection.insertMany(data.rowList)
        );
      });

      return Promise.all(promises);
    })
    .then(() => console.log('Data are successfully seeding into collections'))
    .catch(err => console.error(`Take an error in seeding: ${err}`));
};
