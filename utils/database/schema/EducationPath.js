const EducationPath = {
  name: 'string',
  program: 'string',
  baseEducation: 'Array',
  additionalTest: 'boolean'
};

module.exports = EducationPath;
