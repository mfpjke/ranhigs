const Answer = require('./Answer');
const EducationForm = require('./EducationForm');
const EducationPath = require('./EducationPath');
const EducationPathJoinEducationForm = require('./EducationPathJoinEducationForm');
const EducationPathJoinInterest = require('./EducationPathJoinInterest');
const Interest = require('./Interest');
const Question = require('./Question');
const SchoolSubject = require('./SchoolSubject');
const User = require('./User');
const UserJoinEducationPath = require('./UserJoinEducationPath');

const Schemas = {
  Answer,
  EducationForm,
  EducationPath,
  EducationPathJoinEducationForm,
  EducationPathJoinInterest,
  Interest,
  Question,
  SchoolSubject,
  User,
  UserJoinEducationPath,
};

module.exports = Schemas;
