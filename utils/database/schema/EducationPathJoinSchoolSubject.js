const EducationPathJoinSchoolSubject = {
  educationPathId: 'string',
  schoolSubjectId: 'string',
};

module.exports = EducationPathJoinSchoolSubject;
