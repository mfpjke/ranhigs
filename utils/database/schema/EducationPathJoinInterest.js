const EducationPathJoinInterest = {
  educationPathId: 'string',
  interestId: 'string',
};

module.exports = EducationPathJoinInterest;
