const EducationForm = {
  name: 'string',
  stateFound: 'number',
  byPay: 'number',
  byTarget: 'number',
  byQuote: 'number',
  price: 'number',
};

module.exports = EducationForm;
