const User = {
  email: 'string',
  password: 'string',
  vkToken: 'string',
  twitterToken: 'string',
  facebookToken: 'string',
  googleToken: 'string',
};

module.exports = User;
