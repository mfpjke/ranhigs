const dataContainer = require('./seeds/data/container');
const db = require('../../database');

const migrate = require('./migrations');
const seed = require('./seeds');
const setPointersToEduPath = require('./scripts/setPointersToEduPath');

const collectionNames = Object.keys(dataContainer);
let eduPathCollection = null;

db.init()
  .then(() => {
    eduPathCollection = db.connection.collection('EduPath');
  })
  .then(() => {
    migrate(db, collectionNames);
  })
  .then(() => {
    seed(db, dataContainer);
  })
  .then(() => {
    setPointersToEduPath(db, eduPathCollection);
  })
  .then(() => {
    console.log('Database was expended');
  })
  .catch(err => {
    console.error(err);
  });