const db = require('../../database');
const dataContainer = require('./seeds/data/container');

const collectionNames = Object.keys(dataContainer);

db.init()
  .then(() => {
    const promises = [];

    collectionNames.forEach(el => {
      promises.push(
        db.connection.collection(el).drop()
      );
    });

    return Promise.all(promises);
  })
  .then(() => {
    console.log('Collections are successfully dropped');
  })
  .catch(err => {
    console.error(err);
  });
