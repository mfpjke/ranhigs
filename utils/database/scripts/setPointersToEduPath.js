const ObjectId = require('mongodb').ObjectID;

module.exports = function setPointersToEduPath(db, eduPathCollection) {
  db.checkConnection()
    .then(() => {
      const query = [
        {
          $project: {
            item: '$schoolSubjects',
          }
        },
        {
          $unwind: { path: '$item', preserveNullAndEmptyArrays: true }
        },
        {
          $lookup: {
            from: 'SchoolSubject',
            localField: 'item',
            foreignField: 'name',
            as: 'petuh',
          }
        },
        {
          $unwind: { path: '$petuh', preserveNullAndEmptyArrays: true }
        },
        {
          $project: {
            item: 1,
            pointerSchoolSubject: '$petuh._id',
          }
        }
      ];

      return eduPathCollection.aggregate(query)
        .toArray();
    })
    .then(items => {
      const promises = [];

      items.forEach(item => {

        promises.push(
          eduPathCollection
            .findOneAndUpdate(
              {_id: ObjectId(item._id)},
              {
                $push: { _p_subjects: ObjectId(item.pointerSchoolSubject) },
                $unset: { schoolSubjects: 1 }
              }
            )
        );
      });

      return Promise.all(promises);
    })
    .then(items => {
      const promises = [];

      items.forEach(item => {

        promises.push(
          eduPathCollection
            .findOneAndUpdate(
              {_id: ObjectId(item._id)},
              {
                $push: { _p_interests: ObjectId(item.pointerSchoolSubject) },
                $unset: { interests: 1 }
              }
            )
        );
      });

      return Promise.all(promises);
    })
    .then(() => {
      const query = [
        {
          $project: {
            item: '$eduForms',
          }
        },
        {
          $unwind: { path: '$item', preserveNullAndEmptyArrays: true }
        },
        {
          $lookup: {
            from: 'EduForm',
            localField: 'item',
            foreignField: 'eduForm',
            as: 'lookup',
          }
        },
        {
          $unwind: { path: '$lookup', preserveNullAndEmptyArrays: true }
        },
        {
          $project: {
            item: 1,
            pointerSchoolSubject: '$lookup._id',
          }
        }
      ];

      return eduPathCollection.aggregate(query)
        .toArray();
    })
    .then(items => {
      const promises = [];

      items.forEach(item => {

        promises.push(
          eduPathCollection
            .findOneAndUpdate(
              {_id: ObjectId(item._id)},
              {
                $push: { _p_forms: ObjectId(item.pointerSchoolSubject) },
                $unset: { eduForms: 1 }
              }
            )
        );
      });

      return Promise.all(promises);
    })
};
