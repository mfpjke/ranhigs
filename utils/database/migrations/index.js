module.exports = function migrate(db, collections) {
  db.checkConnection()
    .then(() => {
      const promises = [];

      collections.forEach(el => {
        promises.push(
          db.connection.createCollection(el)
        );
      });

      return Promise.all(promises);
    })
    .catch(err => console.error(`Have trouble with create collections: ${err}`));
};
