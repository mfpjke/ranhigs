require('dotenv').config({ silence: true });
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const cors = require('cors');
const logger = require('morgan');

const options = {
  methods: ['GET', 'PUT', 'POST', 'HEAD', 'DELETE', 'PATCH'],
  allowedHeaders: ['X-Authorization', 'Content-Type'],
};
const loggerStatus = process.env.LOGGER_STATUS || 'dev';

const settings = {
  jsonParser: bodyParser.json(),
  urlEncoded: bodyParser.urlencoded({ extended: true }),
  cookieParser: cookieParser(),
  session: session({ secret: 'extra secret word' }),
  cors: cors(options),
  logger: logger(loggerStatus),
};

module.exports = settings;
