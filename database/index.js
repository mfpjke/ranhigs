const config = require('../config');
const errors = require('../utils/errors');
const MongoClient = require('mongodb').MongoClient;

class MongoDB {
  static init(databaseUri = config.databaseUri) {
    if (!this.connection) {
      return MongoClient.connect(databaseUri, { auto_reconnect: true })
        .then((_db) => {
          this.connection = _db;
        })
        .catch(err => console.error(err));
    }

    return Promise.resolve();
  }

  static checkConnection() {
    return this.connection
      ? Promise.resolve(errors.no_db_connection)
      : Promise.reject();
  }

  static closeConnection() {
    if (this.connection) {
      this.connection.close();
    }

    return Promise.resolve();
  }
}

module.exports = MongoDB;
