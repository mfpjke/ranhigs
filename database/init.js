const db = require('./index');
const config = require('../config');

module.exports = function init() {
  db.init(config.databaseUri)
    .then(() => {
      console.log('Database initialized on 27017 port');
    })
    .catch(err => console.error(err));
};
