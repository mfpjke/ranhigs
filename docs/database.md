[![N|Solid](http://www.ranepa.ru/images/logo.png)](https://sziu.ranepa.ru)

##### Приложение для абитуриентов

---

# Документация

* [Вернуться к README][1]

[1]: ../README.md



### Список коллекций и схемы сущностей

* User
* SchoolSubject
* Interest
* EducationPath
* EducationForm
* Question
* UserJoinEducationPath



##### Описание взаимодействий между сущностями

1. User(many) - EducationPath(many)
2. SchoolSubject(many) - EducationPath(many)
3. Interest(many) - EducationPath(many)
4. EducationForm(many) - EducationPath(many)
5. Question(one) - Answer(many)



##### User

```json
{
  "_id": "string",
  "email": "string",
  "password": "hashed(string)",
  "vkToken": "string",
  "googleToken": "string"
}
```



##### SchoolSubject

```json
{
  "_id": "string",
  "name": "string"
}
```



##### Interest

```json
{
  "_id": "string",
  "name": "string"
}
```



##### EducationPath

```json
{
  "_id": "string",
  "faculty": "string",
  "eduType": "string",
  "name": "string",
  "program": "string",
  "baseEducation": ["string"],
  "additionalTest": "boolean",
  _p_subjects: [ ObjectId ],
  _p_interests: [ ObjectId ],
  _p_forms: [ ObjectId ]
}
```



##### EducationForm

```json
{
  "_id": "string",
  "name": "string",
  "stateFunded": "number",
  "byPay": "number",
  "byTarget": "number",
  "byQuote": "number",
  "price": "number"
}
```



##### Question

```json
{
  "_id": "string",
  "name": "string",
  "answers": [ "string" ]
}
```



##### UserJoinEducationPath

```json
{
  "_id": "string",
  "userId": "string",
  "educationPathId": "string"
}
```
