[![N|Solid](http://www.ranepa.ru/images/logo.png)](https://sziu.ranepa.ru)

##### Приложение для абитуриентов

---

# Документация

* [Вернуться к README][1]

[1]: ../README.md

### Список хранилищ

* SessionStorage
* MiddlewareMatch

##### SessionStorage

```js
sessionStorage[token] = userId;
```

##### middlewareMatch

```js
middlewareMatch[userId] = JSON.stringify(matchedEducationPaths);
```
