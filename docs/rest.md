[![N|Solid](http://www.ranepa.ru/images/logo.png)](https://sziu.ranepa.ru)

##### Приложение для абитуриентов

---

# Документация

* [Вернуться к README][1]

[1]: ../README.md

### REST API

| URL | METHOD | REQUEST | RESPONSE | ACTION |
|-----|--------|---------|----------|--------|
| /login | POST | email, pw | token | auth
| /vk_login | POST | token | token(another) | auth
| /go_login | POST | token | token(another) | auth
| /signup | POST | email, pw | token | reg
| /vk_signup | POST | token | token(another) | reg
| /go_signup | POST | token | token(another) | reg
| /logout | POST | token | status | logout
| /questions | POST | - | array of questions | path selection
| /result | POST | answers | paths | get result of selection
| /search | POST | params | paths | search paths

## Login

Route: */login*

##### Request

```json
{
    "email": "string",
    "password": "string"
}
```

##### Response

```json
{
    "token": "string"
}
```

## Login by VKontakte

Route: */vk_login*

##### Request

```json
{
    "vkToken": "string"
}
```

##### Response

```json
{
    "token": "string"
}
```

## Login by Google

Route: */go_login*

##### Request

```json
{
    "goToken": "string"
}
```

##### Response

```json
{
    "token": "string"
}
```

## Signup

Route: */signup*

##### Request

```json
{
    "email": "string",
    "password": "string"
}
```

##### Response

```json
{
    "token": "string"
}
```

## Signup by VKontakte

Router: */vk_signup*

##### Request

```json
{
    "vkToken": "string"
}
```

```json
{
    "token": "string"
}
```

## Signup by Google

Router: */go_signup*

##### Request

```json
{
    "goToken": "string"
}
```

```json
{
    "token": "string"
}
```

## Logout

Route: */logout*

##### Request

```json
{
    "token": "string"
}
```

##### Response

```json
{
    "status": "string"
}
```

## Get all questions

Route: */questions*

##### Request

```json
{
    "token": "string",
    "type": "string"
}
```

*type:*

* 9grade
* 11grade
* bachelor
* master

##### Response

```json
{
    "questions": [
        "_id": "string",
        "multy": "boolean",
        "text": "string",
        "answers": [
            { "text": "string" }
        ]
    ]
}
```

## Get results of algorithm work

Route: */result*

##### Request

```json
{
    "token": "string",
    "type": "string",
    "questions": [
        "_id": "string",
        "answers": [
            { "text": "string" }
        ]
    ]
}
```


*type:*

* 9grade
* 11grade
* bachelor
* master

*form* - i mean type of form:

* Очная
* Очно-заочная
* Заочная

##### Response

```json
{
    "token": "string",
    "educationPaths": [
        {
            "path": {
                "faculty": "string",
                "title": "string",
                "programs": [
                    { "name": "string" }
                ],
                "baseEducation": [
                    { "name": "string" }
                ],
                "additionalTest": "boolean",
                "schoolSubjects": [
                    { "name": "string" }
                ],
                "forms": [
                    {
                        "type": "string",
                        "seats": "number",
                        "byTarget": "number",
                        "byQuote": "number",
                        "byCash": "number",
                        "price": "number",
                    }
                ]
            }
        }
    ]
}
```

## Search education path

Route: */search*

##### Request

```json
{
    "token": "string",
    "type": "string",
    "price": "number",
    "query": "string",
    "schoolSubjects": [
        { "name": "string" }
    ]
}
```


*type:*

* 9grade
* 11grade
* bachelor
* master

##### Response

```json
{
    "token": "string",
    "educationPaths": [
        {
            "path": {
                "faculty": "string",
                "title": "string",
                "programs": [
                    { "name": "string" }
                ],
                "baseEducation": [
                    { "name": "string" }
                ],
                "additionalTest": "boolean",
                "schoolSubjects": [
                    { "name": "string" }
                ],
                "forms": [
                    {
                        "type": "string",
                        "places": "number",
                        "byTarget": "number",
                        "byQuote": "number",
                        "byMoney": "number",
                        "price": "number",
                    }
                ]
            }
        }
    ]
}
```
